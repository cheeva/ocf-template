PROC_NAME=wd.sapFOP
PIDFILE=/var/log/ocf-monitor.pid
while true
do
        if [ -n "$PIDFILE" -a -f $PIDFILE ]; then
                # processes is probably running
                PID=`cat $PIDFILE`
                if [ -n "$PID" ]; then
                        if [ $(ps -p $PID|grep -v "PID"|wc -l) -gt 0 ]; then
				echo "####################"
                                echo "processes is running"
				echo "####################"
#                                return $OCF_SUCCESS
                        else
				echo "############################################"
                                echo "processes is not running but pid file exists"
				echo "############################################"
#                                return $OCF_ERR_GENERIC
                        fi
                else
			echo "###############"
                        echo "PID file empty!"
			echo "###############"
#                        return $OCF_ERR_GENERIC
                fi
	else
		echo "############"
		echo "No PID file!"
		echo "############"
#		return $OCF_NOT_RUNNING
        fi
ps -ef|grep -i $PROC_NAME
cat $PIDFILE
sleep 3
clear
done
