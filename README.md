# Corosync Pacemaker - OCF Template

## Install Steps

Copy start-service.sh and stop-service.sh to path as your choose:

Example: /scripts

    # cp start-service.sh /scripts/
    # cp stop-service.sh /scripts/

Copy the ocf-template resource over to a new resource.

    # cp -Rp ocf-template /usr/lib/ocf/resource.d/heartbeat/
    # chmod 755 /usr/lib/ocf/resource.d/heartbeat/ocf-template

Replace all instances of Dummy and dummy with your name of choice:    

    # sed -i 's/Dummy/<Your service name>/g' /usr/lib/ocf/resource.d/heartbeat/ocf-template
    # sed -i 's/dummy/<Your service name>/g' /usr/lib/ocf/resource.d/heartbeat/ocf-template

## Set parameter to start/stop scripts

Edit Parameter PROC_NAME and PIDFILE in start-service.sh and stop-service.sh

PROC_NAME=Name of processes for monitor

PIDFILE=File for keep processes ID for monitor

Example: 

PROC_NAME=/usr/sap/WDP/wd.sapFOP

PIDFILE=/var/log/ocf-monitor.pid

    # vi start-service.sh
    # vi stop-service.sh
    
    PROC_NAME=/usr/sap/WDP/wd.sapFOP
    
    PIDFILE=/var/log/ocf-monitor.pid

Add start/stop script in start-service.sh and stop-service.sh

    # vi start-service.sh

    #------------------------------ script start ------------------------------#
    # Add command or start script in this section
    bash -c "exec -a $PROC_NAME$i sleep 100000" &
    #--------------------------------------------------------------------------#

    # vi stop-service.sh

    #------------------------------ script stop ------------------------------#
    # Add command or stop script in this section
    ps -ef|grep -i $PROC_NAME|grep -v grep|awk '{print $2}'|xargs -i kill -9 {}
    #-------------------------------------------------------------------------#

## Set parameter to ocf-template

Edit Paramater OCF_RESKEY_state

OCF_RESKEY_state parameter should same as PIDFILE parameter in start-service.sh and stop-service.sh

    # vi ocf-template

    #---------- Set Parameter for PIDFILE ----------#
    : ${OCF_RESKEY_state=/var/log/ocf-monitor.pid}
    #-----------------------------------------------#


Add start/stop scripts to ocf-template

    # vi ocf-template

    In start section

    #---------- script start ----------#
    /scripts/start-service.sh
    #----------------------------------#

    In stop section
    
    #---------- script stop -----------#
    /scripts/stop-service.sh
    #----------------------------------#

# Optional: Change resource name with your name of choose: 

    # mv /usr/lib/ocf/resource.d/heartbeat/ocf-template /usr/lib/ocf/resource.d/heartbeat/<Your resource name>

# Add resouce to cluster and Test


Add ocf primitives resource application to cluster

Test start resource, kill processes and monitor



## License

MIT / BSD



## Author Information



This OCF template was created in 2018 by [Cheeva Rubsrisuksakul].
