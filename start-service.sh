PROC_NAME=/usr/sap/WDP/wd.sapFOP
PIDFILE=/var/log/ocf-monitor.pid

if [ $(ps -ef|grep -i $PROC_NAME|grep -v grep|wc -l) -gt 0 ]
then
	echo "######################################################"
	echo "processes $PROC_NAME already exists"
	echo "######################################################"
else
	if [ -f $PIDFILE ]
	then
		rm -rf $PIDFILE
	fi
#------------------------------ script start ------------------------------#
# Add command or start script in this section
# bash -c "exec -a $PROC_NAME$i sleep 100000" &
#--------------------------------------------------------------------------#
fi
ps -ef|grep -i $PROC_NAME|grep -v grep|awk '{print $2}' > $PIDFILE
