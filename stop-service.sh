PROC_NAME=wd.sapFOP
PIDFILE=/var/log/ocf-monitor.pid

#------------------------------ script stop ------------------------------#
# Add command or stop script in this section
# ps -ef|grep -i $PROC_NAME|grep -v grep|awk '{print $2}'|xargs -i kill -9 {}
#-------------------------------------------------------------------------#

rm -rf $PIDFILE
